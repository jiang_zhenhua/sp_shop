import { PropType } from 'vue'
export const IconProps = {
  color: {
    type: String,
    default: ''
  },
  size: {
    type: Number as PropType<number>,
    default: 30
  }
}