import { ref, watch } from 'vue'
export function useIconfont(props) {
  const colors = ref('')
  const svgSize = ref(30 / 750 * uni.getSystemInfoSync().windowWidth)
  const quot = ref('"')
  const isStr = ref(true)

  watch(() => props.size, (s) => {
    svgSize.value = s / 750 * uni.getSystemInfoSync().windowWidth
  }, { immediate: true })

  watch(() => props.color, (c) => {
    colors.value = fixColor(c)
    isStr.value = typeof c === 'string'
  }, { immediate: true })

  function fixColor(c: string) {
    const color = c
    const hex2rgbFn = hex2rgb

    if (typeof color === 'string') {
      return color.indexOf('#') === 0 ? hex2rgbFn(color) : color
    }

    return color.map(function (item) {
      return item.indexOf('#') === 0 ? hex2rgbFn(item) : item
    })
  }

  function hex2rgb(hex) {
    const rgb = []
  
    hex = hex.substr(1)
  
    if (hex.length === 3) {
      hex = hex.replace(/(.)/g, '$1$1')
    }
  
    hex.replace(/../g, function(color) {
      rgb.push(parseInt(color, 0x10))
      return color
    })
  
    return 'rgb(' + rgb.join(',') + ')'
  }
  return {
    fixColor,
    colors,
    svgSize,
    quot,
    isStr,
  }
}