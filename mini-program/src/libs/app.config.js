/**
 * 应用配置
 */
export default {
	//请求配置
	requestConfig: {
		//超时时间
		timeout: 5000,
		//是否携带请求信息
		withCredentials: true,
		//请求地址
		webServiceUrl: 'http://localhost:8080/api/',
		//请求时是否显示加载
		showLoading: true,
		//是否刷新token
		isRefreshing: true,
		//刷新地址
		refreshUrl: "login/refreshToken",
		// 登录地址
		loginUrl: 'login/code',
		//配置是否刷新令牌
		needRefresh: true
	},
	//缓存配置
	storageConfig: {
		//前缀
		prefix: "user_",
		//访问令牌名称
		accessTokenName: "token",
		//刷新令牌名称
		refreshTokenName: "refreshToken",
	},
	//配置响应码各响应码作用
	//执行顺序 showMessage->storageClear->backToPrevious
	returnHandleConfig: {
		//提示错误的响应码
		showMessage: [
			103,
			104,
			105,
			106,
		],
		//清理缓存响应码
		storageClear: [],
		//返回上一级响应码
		backToPrevious: [],
		//无效的访问
		invalidAccessToken: [
			-1,
		]
	}
}
