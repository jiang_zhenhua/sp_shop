/**
 * 保留当前页面，跳转到应用内的某个页面，使用uni.navigateBack可以返回到原页面。
 */
export function nav (url,opts={}) {
    uni.navigateTo({
        url: url,
        ...opts
    });
}

/**
 * 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面。
 */
export const swi = (url: string) => {
	uni.switchTab({
		url: url
	});
}
/**
 * 关闭所有页面，打开到应用内的某个页面
 */
export const rel = (url: string) => {
	uni.reLaunch({
	  url: url,
	  fail: (e) => {
		console.error(e);
	  }
	});
  }
/**
 * 关闭当前页面，跳转到应用内的某个页面。
 */
export const red = (url: string) => {
	uni.redirectTo({
	  url: url,
	  fail: (e) => {
		console.error(e);
	  }
	});
}
/**
 * 关闭当前页面，返回上一页面或多级页面。可通过 getCurrentPages() 获取当前的页面栈，决定需要返回几层。
 */
export const back = (delta:number) => {
	uni.navigateBack({
	  delta: delta
	});
}
/**
 * 保留小数后几位
 *
 * @param num 数值
 * @param decimalPlaces 指定小数后几位
 * @param round 是否进行四舍五入，default true
 * @returns
 */
export function roundToDecimal(num: number, decimalPlaces: number, round = true): string {
	if(num===undefined){
		return Number(0).toFixed(decimalPlaces);
	}
	const multiplier = Math.pow(10, decimalPlaces)
	if (round) {
		return (Math.round(num * multiplier) / multiplier).toFixed(decimalPlaces)
	}
	return (Math.floor(num * multiplier) / multiplier).toFixed(decimalPlaces)
}
/**
 * 方法防抖
 * @param func 
 * @param delay 
 * @returns 
 */
let timer:any = null;
export function debounce(func, delay=1000) {
    clearTimeout(timer);
    timer = setTimeout(() => {
        func.apply(this, arguments);
    }, delay);
}
/**
 * 格式化时间
 */
export function dateFormat(date,fmt="yyyy-MM-dd HH:mm:ss") {
    if(!(date instanceof Date)){
        date = new Date(date*1000);
    }
    let ret
    const opt = {
        'y+': date.getFullYear().toString(), // 年
        'M+': (date.getMonth() + 1).toString(), // 月
        'd+': date.getDate().toString(), // 日
        'H+': date.getHours().toString(), // 时
        'm+': date.getMinutes().toString(), // 分
        's+': date.getSeconds().toString(), // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    }
    for (let k in opt) {
        ret = new RegExp('(' + k + ')').exec(fmt)
        if (ret) {
            fmt = fmt.replace(
                ret[1],
                ret[1].length == 1 ? opt[k] : opt[k].padStart(ret[1].length, '0')
            )
        }
    }
    return fmt
};