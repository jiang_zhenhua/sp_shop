<template>
    <view class="car-page">
        <view class="header">
            <view class="count">
                共<text>{{cartTotal.totalQuantity}}</text>件商品
            </view>
            <view class="active">
                <template v-if="!isEdit">
                    <view @click="isEdit=!isEdit">
                        <uni-icons type="compose" size="20"></uni-icons>
                        <span>编辑</span>
                    </view>
                </template>
                <template v-else>
                    <span @click="isEdit=!isEdit">完成</span>
                </template>
            </view>
        </view>
        <view class="main">
            <view class="card-item" v-for="(item,index) in list" :key="index" v-if="list.length">
                <uni-swipe-action>
                    <uni-swipe-action-item :right-options="options" @click="handelDeleteItem(item,index)"  @change="handelChangeItem($event,item)">
                        <view class="card-content">
                            <sp-checkbox :checked="item.checked" @change="handleChangeCheckBox(item)"></sp-checkbox>
                            <view class="image-box" @click="nav(`/subpackage/goods/pages/detail/index?id=${item.goodsId}`)">
                                <up-image 
                                :show-loading="true" 
                                width="143rpx" 
                                height="143rpx"
                                radius="15rpx"
                                :src="item.cover"></up-image>
                                <text class="tips" v-if="item.storeCount<item.quantity">库存不足</text>
                                <text class="tips" v-if="!item.status">商品下架</text>
                            </view>
                            <view class="info">
                                <text class="name">{{item.goodsName}}</text>
                                <text class="spec"> 颜色:{{item.keyName}}</text>
                                <view class="active">
                                    <view class="price-wrapper">
                                        <text class="unit">￥</text>
                                        <text class="value">{{roundToDecimal(item.price,2)}}</text>
                                    </view>
                                    <u-number-box disabledInput v-model="item.quantity" @change="handleNumber(item)">    
                                        <template #input>
                                            <text style="width: 80rpx;text-align: center;">{{item.quantity}}</text>
                                        </template>
                                    </u-number-box>
                                </view>
                            </view>
                        </view>
                    </uni-swipe-action-item>
                </uni-swipe-action>
            </view>
            <u-empty mode="car" v-else></u-empty>
        </view>
        <view class="footer" v-if="cartTotal.totalQuantity!=0">
            <view class="all-select">
                <sp-checkbox 
                    :checked="cartTotal.totalQuantity!=0 && cartTotal.totalQuantity==cartTotal.selectedQuantity" 
                    @change="handleChangeAll"/>
                <view class="l-label">全选</view>
            </view>
            <view class="count-container">
                <view class="price-wrapper">
                    <text>合计:</text>
                    <text class="unit">￥</text>
                    <text class="value">{{roundToDecimal(cartTotal.totalAmount,2)}}</text>
                </view>
                <view class="count-wrapper">已选{{cartTotal.selectedQuantity}}件</view>
            </view>
            <view class="active">
                <view class="button" v-if="!isEdit" @click="cartTotal.selectedQuantity && nav(`/subpackage/order/pages/submit/index`)" :class="{disabled:!cartTotal.selectedQuantity}">提交订单</view>
                <view class="button" v-if="isEdit" @click="handelDeleteBatch">删除</view>
            </view>
        </view>
        <view class="guess-like-container">
		    <sp-guess-like v-if="!query.isNext"></sp-guess-like>
        </view>
    </view>
</template>
<script setup lang="ts">
import { ref,reactive } from "vue"
import { get, post } from "@/libs/request";
import { onShow, onReachBottom, onPullDownRefresh } from "@dcloudio/uni-app";
import { roundToDecimal, debounce, nav } from "@/libs/utils"
const query = reactive<any>({
    pageIndex:1,
    pageSize:10,
    isNext:true,
});
const list = ref<any>([]);
const isEdit = ref<boolean>(false);
const options = [{
    text: "删除",
    style: {
      backgroundColor: "#FF0005",
    },
}];
const cartTotal = ref<any>({
    totalQuantity:0,
    selectedQuantity:0,
    totalAmount:0.00,
});
/**
 * 获取购物车合计
 */
 const getCartTotal = ()=>{
    get(`car/getCartTotal`).then((res:any)=>{
        if(res.errCode==0){
            cartTotal.value = res.data;
        }
    })
}
/**
 * 获取购物车分页
 */
const getList = ()=>{
    post("car/paginate",{
        pageIndex: query.pageIndex,
        pageSize: query.pageSize
    }).then((res:any)=>{
        if(res.errCode==0){
            list.value = list.value.concat(res.data);
            query.pageIndex++;
            if(list.value.length >= res.page.total){
                query.isNext = false;
            }
        }
    })
}
/**
 * 点击修改数量
 * @param item 
 */
const handleNumber = (item:any)=>{
    debounce(()=>{
        post("car/editQuantity",{},{
            carId: item.carId,
            quantity: item.quantity
        }).then((res:any)=>{
            if(res.errCode==0){
                cartTotal.value = res.data;
            }
        })
    })
}
/**
 * 修改选中状态
 * @param item 
 */
const handleChangeCheckBox = (item:any)=>{
    get(`car/editChecked/${item.carId}`).then((res:any)=>{
        if(res.errCode==0){
            cartTotal.value = res.data;
        }
    })
}
/**
 * 删除单条记录
 */
const handelDeleteItem = (item:any,index:number)=>{
    if(item.type === "right"){
        get(`car/delete/${item.carId}`).then((res:any)=>{
            if(res.errCode==0){
                cartTotal.value = res.data;
                list.value.splice(index,1)
            }
        })
    }
}
/**
 * 切换状态
 * @param e 
 * @param item 
 */
const handelChangeItem = (e:any,item:any)=>{
    item.type = e;
}
/**
 * 全部选中或取消
 */
const handleChangeAll = ()=>{
    let checked = cartTotal.value.totalQuantity==cartTotal.value.selectedQuantity?false:true;
    post(`car/changeAll`,{
        checked
    }).then((res:any)=>{
        if(res.errCode==0){
            list.value = list.value.map((item:any)=>{
                item.checked = checked;
                return item;
            })
            cartTotal.value = res.data;
        }
    })
}
/**
 * 批量删除
 */
const handelDeleteBatch = ()=>{
    post(`car/deleteBatch`,{},{
        carIds:list.value.map((item:any)=>item.carId)
    }).then((res:any)=>{
        if(res.errCode==0){
            reload();
        }
    })
}
/**
 * 重置
 */
const reload = async()=>{
    list.value = [];
    query.pageIndex = 1;
    query.isNext = true;
    await getList();
    await getCartTotal();
}
onReachBottom(() => {
  query.isNext && getList()
})
onShow(async() => {
    await reload()
})
onPullDownRefresh(async()=>{
    await reload();
    uni.stopPullDownRefresh();
})
</script>
<style scoped lang="less">
.car-page{
    height: 100vh;
    background: #f7f7f7;
    overflow: auto;
    .header{
        padding: 0 20rpx;
        height: 100rpx;
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 30rpx;
        .count{
            text{
                color: #ff0000;
            }
        }
        .active{
            display: flex;
            align-items: center;
        }
    }
    .main{
        display: flex;
        flex-flow: column;
        align-content: center;
        justify-content: center;
        padding: 0 15rpx;
        .card-item{
            display: flex;
            align-items: center;
            padding: 30rpx 20rpx 20rpx;
            margin-bottom: 20rpx;
            background: #fff;
            border-radius: 20rpx;
            .card-content{
                display: flex;
                align-items: center;
                .image-box{
                    position: relative;
                    margin: 0 19rpx 0 17rpx;
                    .tips{
                        position: absolute;
                        top: 50%;
                        left: 50%;
                        transform: translate(-50%,-50%);
                        background: rgba(0, 0, 0, 0.3);
                        color: #FFFFFF;
                        font-size: 24rpx;
                        width: 120rpx;
                        border-radius: 15rpx;
                        text-align: center;
                    }
                }
                .info{
                    width: 470rpx;
                    .name{
                        display: -webkit-box;
                        margin: 0 0 9rpx;
                        height: 38rpx;
                        line-height: 38rpx;
                        color: #323232;
                        font-size: 30rpx;
                        font-weight: 500;
                        overflow: hidden;
                        text-overflow: ellipsis;
                        margin-bottom: 10rpx;
                    }
                    .spec{
                        display: inline-block;
                        font-size: 24rpx;
                        background: #f7f8fa;
                        color: #c8c9cc;
                        padding: 10rpx;
                        border-radius: 15rpx;
                    }
                    .active{
                        margin-top: 20rpx;;
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                        .price-wrapper{
                            .unit{
                                display: inline-block;
                                color: #ff0000;
                                font-size: 24rpx;
                            }
                            .value{
                                display: inline-block;
                                color: #ff0000;
                                font-size: 30rpx;
                            }
                        }
                    }
                }
            }
        }
    }
    .footer{
        position: fixed;
        bottom: 0;
        z-index: 99;
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100vw;
        height: 90rpx;
        background: #fff;
        box-shadow: 0px -2rpx 3rpx 0px rgba(24, 24, 24, 0.05);
        padding: 0 20rpx;
        box-sizing: border-box;
        .all-select{
            display: flex;
            align-items: center;
            .l-label{
                margin-left: 5rpx;
            }
        }
        .count-container{
            display: flex;
            flex:auto;
            flex-direction:column;
            padding: 0 20rpx;
            .price-wrapper{
                display: flex;
                align-items: center;
                justify-content: flex-end;
                .unit{
                    margin-left: 10rpx;
                    display: inline-block;
                    color: #ff0000;
                    font-size: 26rpx;
                }
                .value{
                    display: inline-block;
                    color: #ff0000;
                    font-size: 34rpx;
                    box-sizing: border-box
                }
            }
            .count-wrapper{
                text-align: right;
                font-size: 24rpx;
                color: #666;
            }
        }
        .active{
            .disabled{
                opacity: 0.6;
            }
            .button{
                width: 170rpx;
                background: linear-gradient(90deg, rgb(254, 96, 53), rgb(239, 18, 36));
                color: rgb(255, 255, 255);
                padding: 15rpx 30rpx;
                border-radius: 35rpx;
                text-align: center;
            }
        }
    }
    .guess-like-container{
        padding: 0 20rpx 90rpx;
    }
}
</style>