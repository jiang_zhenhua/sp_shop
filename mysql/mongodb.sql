/*
 Navicat Premium Data Transfer

 Source Server         : localhostMongoDB
 Source Server Type    : MongoDB
 Source Server Version : 70009
 Source Host           : localhost:27017
 Source Schema         : sp_shop

 Target Server Type    : MongoDB
 Target Server Version : 70009
 File Encoding         : 65001

 Date: 26/02/2025 16:19:14
*/


// ----------------------------
// Collection structure for browsing_history
// ----------------------------
db.getCollection("browsing_history").drop();
db.createCollection("browsing_history");

// ----------------------------
// Documents of browsing_history
// ----------------------------
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("6659481cc3744a5d2274ac12"),
    userId: NumberInt("1"),
    goodsId: NumberInt("6"),
    goodsName: "机械革命",
    goodsCover: "http://172.17.165.79:8080/static/uploads/2024-05/1715051634734.png",
    price: "0.00",
    updateTime: NumberInt("1740125256"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66594830c3744a5d2274ac13"),
    userId: NumberInt("1"),
    goodsId: NumberInt("11"),
    goodsName: "哆啦A梦",
    goodsCover: "http://172.17.165.79:8080/static/uploads/2024-05/1715049601218.png",
    price: "99999.00",
    updateTime: NumberInt("1739411852"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("6659732ac3744a5d2274ac14"),
    userId: NumberInt("1"),
    goodsId: NumberInt("7"),
    goodsName: "小米",
    goodsCover: "http://172.17.165.79:8080/static/uploads/2024-05/1715049684657.jpg",
    price: "10.00",
    updateTime: NumberInt("1740550684"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66597364c3744a5d2274ac15"),
    userId: NumberInt("1"),
    goodsId: NumberInt("13"),
    goodsName: "华为Mate60",
    goodsCover: "http://172.17.165.79:8080/static/uploads/2024-05/1715049517105.jpg",
    price: "6999.00",
    updateTime: NumberInt("1717138736"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66597526c3744a5d2274ac16"),
    userId: NumberInt("1"),
    goodsId: NumberInt("10"),
    goodsName: "三星s24",
    goodsCover: "http://172.17.165.79:8080/static/uploads/2024-05/1715049690969.jpg",
    price: "4999.00",
    updateTime: NumberInt("1739254307"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66616131a2e1cf5be34c0ac4"),
    userId: NumberInt("1"),
    goodsId: NumberInt("14"),
    goodsName: "苹果/iphone11",
    goodsCover: "http://172.17.165.79:8080/static/uploads/2024-05/1715049443916.jpg",
    price: "6799.00",
    updateTime: NumberInt("1740124015"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66de632bd643c93a9263b989"),
    userId: NumberInt("1"),
    goodsId: NumberInt("15"),
    goodsName: "霸气橙子",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1725849724680.png",
    updateTime: NumberInt("1731480263"),
    _class: "com.example.common.po.BrowsingHistory",
    price: "1.00"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66de705dd643c93a9263b98a"),
    userId: NumberInt("1"),
    goodsId: NumberInt("16"),
    goodsName: "霸气葡萄",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1725851007782.png",
    updateTime: NumberInt("1726383285"),
    _class: "com.example.common.po.BrowsingHistory",
    price: "0.00"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66e4147621712f42f223f7de"),
    userId: NumberInt("1"),
    goodsId: NumberInt("19"),
    goodsName: "银河有迹可循",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1726220657135.png",
    updateTime: NumberInt("1727667268"),
    _class: "com.example.common.po.BrowsingHistory",
    price: null
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66f9ff83f7e082442ae7ad0a"),
    userId: NumberInt("1"),
    goodsId: NumberInt("32"),
    goodsName: "相信你是爱我的",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1726221545151.png",
    updateTime: NumberInt("1727659907"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66fa1903214c0f12f5d98fbd"),
    userId: NumberInt("1"),
    goodsId: NumberInt("18"),
    goodsName: "着迷于你眼睛",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1726220638041.png",
    updateTime: NumberInt("1727688952"),
    _class: "com.example.common.po.BrowsingHistory",
    price: "0.00"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66fa1915214c0f12f5d98fbe"),
    userId: NumberInt("1"),
    goodsId: NumberInt("21"),
    goodsName: "它依然真实地",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1726220693663.png",
    updateTime: NumberInt("1727666453"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66fa1cb7214c0f12f5d98fbf"),
    userId: NumberInt("1"),
    goodsId: NumberInt("20"),
    goodsName: "穿过时间的缝隙",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1726220678674.png",
    updateTime: NumberInt("1730193574"),
    _class: "com.example.common.po.BrowsingHistory",
    price: null
} ]);
db.getCollection("browsing_history").insert([ {
    _id: ObjectId("66fa1cc4214c0f12f5d98fc0"),
    userId: NumberInt("1"),
    goodsId: NumberInt("17"),
    goodsName: "多讨厌的雨天",
    goodsCover: "http://127.0.0.1:8181/static/uploads/2024-09/1725869647465.png",
    updateTime: NumberInt("1727667396"),
    _class: "com.example.common.po.BrowsingHistory"
} ]);
