package com.example.api.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.api.vo.UserCountVO;
import com.example.api.service.GoodsCollectService;
import com.example.api.service.OrderService;
import com.example.api.service.UserService;
import com.example.common.enums.OrderStatus;
import com.example.common.mapper.UserMapper;
import com.example.common.po.BrowsingHistoryPO;
import com.example.common.po.GoodsCollectPO;
import com.example.common.po.OrderPO;
import com.example.common.po.UserPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 *
 */
@Service("apiUserServiceImpl")
public class UserServiceImpl extends ServiceImpl<UserMapper, UserPO>
        implements UserService {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsCollectService goodsCollectService;

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 获取统计数量
     *
     * @return
     */
    @Override
    public UserCountVO getCount() {
        Integer userId = (Integer) httpServletRequest.getSession().getAttribute("id");
        UserCountVO userCountVO = new UserCountVO();
        // 待支付
        userCountVO.setWaitPayCount((int) orderService.count(new QueryWrapper<OrderPO>().lambda().eq(OrderPO::getUserId, userId).eq(OrderPO::getOrderStatus, OrderStatus.WAIT_PAY.getKey())));
        // 待发货
        userCountVO.setWaitSendCount((int) orderService.count(new QueryWrapper<OrderPO>().lambda().eq(OrderPO::getUserId, userId).eq(OrderPO::getOrderStatus, OrderStatus.WAIT_SEND.getKey())));
        // 待收货
        userCountVO.setWaitTakeCount((int) orderService.count(new QueryWrapper<OrderPO>().lambda().eq(OrderPO::getUserId, userId).eq(OrderPO::getOrderStatus, OrderStatus.WAIT_TAKE.getKey())));
        // 售后
        userCountVO.setAfterSales((int) orderService.count(new QueryWrapper<OrderPO>().lambda().eq(OrderPO::getUserId, userId).eq(OrderPO::getOrderStatus, OrderStatus.AFTER_SALES.getKey())));
        // 收藏商品数量
        userCountVO.setCollectCount((int) goodsCollectService.count(new QueryWrapper<GoodsCollectPO>().lambda().eq(GoodsCollectPO::getUserId, userId).eq(GoodsCollectPO::getDeleteTime, 0)));
        // 浏览记录数量
        userCountVO.setBrowsingHistoryCount((int) mongoTemplate.count(new Query(), BrowsingHistoryPO.class));
        return userCountVO;
    }
}




