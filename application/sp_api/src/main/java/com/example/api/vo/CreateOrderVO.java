package com.example.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建订单
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrderVO {
    @ApiModelProperty(value = "订单id", example = "1")
    private Integer orderId;
}
