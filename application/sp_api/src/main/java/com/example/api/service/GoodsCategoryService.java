package com.example.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.vo.GoodsCategoryTreeVO;
import com.example.common.po.GoodsCategoryPO;

import java.util.List;

/**
 *
 */
public interface GoodsCategoryService extends IService<GoodsCategoryPO> {
    List<GoodsCategoryTreeVO> getTree();
}
