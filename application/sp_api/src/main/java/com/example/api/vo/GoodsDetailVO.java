package com.example.api.vo;

import com.alibaba.fastjson2.JSONArray;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsDetailVO {
    @ApiModelProperty(value = "商品id", example = "1")
    private Integer goodsId;

    @ApiModelProperty(value = "商品名称", example = "半夏商城")
    private String goodsName;

    @ApiModelProperty(value = "简介", example = "")
    private String goodsDesc;

    @ApiModelProperty(value = "商品封面", example = "url")
    private String goodsCover;

    @ApiModelProperty(value = "画册", example = "url")
    private JSONArray goodsGallery;

    @ApiModelProperty(value = "详细描述", example = "")
    private String goodsContent;

    @ApiModelProperty(value = "单位", example = "台")
    private String unitName;

    @ApiModelProperty(value = "是否热门", example = "true")
    private Boolean isHot;

    @ApiModelProperty(value = "是否新品", example = "true")
    private Boolean isNew;

    @ApiModelProperty(value = "最低规格价格", example = "0.1")
    private BigDecimal price;

    @ApiModelProperty(value = "收藏id", example = "1")
    private Integer collectId;

    @ApiModelProperty(value = "属性", example = "")
    private List<GoodsDetailAttrVO> attr;

    @ApiModelProperty(value = "规格组合", example = "")
    private List<GoodsDetailSpecVO> spec;
}
