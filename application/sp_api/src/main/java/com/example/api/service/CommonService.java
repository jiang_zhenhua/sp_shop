package com.example.api.service;

import com.example.api.dto.CommonGetMesDTO;

public interface CommonService {
    void getMes(CommonGetMesDTO commonGetMesDTO) throws Exception;
}
