package com.example.api.dto;

import com.example.common.annotation.MobileValidate;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonGetMesDTO {
    @ApiModelProperty(value = "phone", example = "13200000001")
    @NotNull(message = "请输入手机号码")
    @MobileValidate
    private String mobile;

}
