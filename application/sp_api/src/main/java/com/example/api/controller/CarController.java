package com.example.api.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.api.dto.CarAddDTO;
import com.example.api.dto.CarEditQuantityDTO;
import com.example.api.po.CarPaginatePO;
import com.example.api.service.CarService;
import com.example.common.bo.PageParamBO;
import com.example.common.vo.PageResultVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/car")
@Api(tags = "购物车")
public class CarController {

    @Autowired
    private CarService carService;

    /**
     * 分页
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @ApiOperation("分页")
    @PostMapping("/paginate")
    public ResultDataVO<List<CarPaginatePO>> paginate(
            @ApiParam(name = "pageIndex", value = "当前页数", required = true) @RequestParam Integer pageIndex,
            @ApiParam(name = "pageSize", value = "每页显示条目个数", required = true) @RequestParam Integer pageSize
    ) {
        IPage page = carService.getJoinPaginateById(new PageParamBO(pageIndex, pageSize));
        //构建响应对象
        return ResultDataVO.success(page.getRecords(), new PageResultVO(page));
    }

    /**
     * 添加
     *
     * @param carAddDto
     * @return
     */
    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultDataVO add(@Validated @RequestBody CarAddDTO carAddDto) {
        return ResultDataVO.success(carService.add(carAddDto));
    }

    /**
     * 获取数量
     *
     * @return
     */
    @ApiOperation("获取数量")
    @GetMapping("/getCount")
    public ResultDataVO getCount() {
        HashMap<String, Object> result = new HashMap();
        result.put("count", carService.getCount());
        return ResultDataVO.success(result);
    }

    /**
     * 修改购物车商品数量
     *
     * @param carEditQuantityDto
     * @return
     */
    @ApiOperation("修改数量")
    @PostMapping("/editQuantity")
    public ResultDataVO editQuantity(@Validated @RequestBody CarEditQuantityDTO carEditQuantityDto) {
        return ResultDataVO.success(carService.editQuantity(carEditQuantityDto));
    }

    /**
     * 修改选中状态
     *
     * @param carId
     * @return
     */
    @ApiOperation("修改选中状态")
    @GetMapping("/editChecked/{id}")
    public ResultDataVO editChecked(
            @ApiParam(name = "id", value = "商品id", required = true) @PathVariable("id") Integer carId
    ) {
        return ResultDataVO.success(carService.editChecked(carId));
    }

    /**
     * 获取购物车合计
     *
     * @return
     */
    @ApiOperation("获取购物车合计")
    @GetMapping("/getCartTotal")
    public ResultDataVO getCartTotal() {
        return ResultDataVO.success(carService.getCartTotal());
    }

    /**
     * 修改全部选中状态
     *
     * @param checked
     * @return
     */
    @ApiOperation("修改全部选中状态")
    @PostMapping("/changeAll")
    public ResultDataVO changeAll(
            @ApiParam(name = "checked", value = "选中状态", required = true) @RequestParam("checked") Boolean checked
    ) {
        return ResultDataVO.success(carService.changeAll(checked));
    }

    /**
     * 删除购物车商品
     *
     * @return
     */
    @ApiOperation("删除购物车商品")
    @GetMapping("/delete/{id}")
    public ResultDataVO delete(
            @ApiParam(name = "id", value = "1", required = true) @PathVariable("id") Integer carId
    ) {
        return ResultDataVO.success(carService.delete(carId));
    }

    /**
     * 批量删除
     *
     * @param carIds
     * @return
     */
    @ApiOperation("批量删除")
    @PostMapping("/deleteBatch")
    public ResultDataVO deleteBatch(
            @ApiParam(name = "carIds", value = "[1,2,3]", required = true) @RequestParam(value = "carIds", required = true) List<Integer> carIds
    ) {
        return ResultDataVO.success(carService.deleteBatch(carIds));
    }
}
