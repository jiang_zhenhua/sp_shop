package com.example.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.dto.GoodsCollectAddDTO;
import com.example.api.dto.GoodsCollectCancelDTO;
import com.example.common.po.GoodsCollectPO;

/**
 *
 */
public interface GoodsCollectService extends IService<GoodsCollectPO> {
    Integer add(GoodsCollectAddDTO goodsCollectAddDTO);

    Integer cancel(GoodsCollectCancelDTO goodsCollectCancelDTO);
}
