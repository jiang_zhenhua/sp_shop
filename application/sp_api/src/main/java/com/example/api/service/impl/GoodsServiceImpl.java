package com.example.api.service.impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.api.bo.BrowsingHistoryBO;
import com.example.api.dto.GoodsPaginateDTO;
import com.example.api.mapper.GoodsMapper;
import com.example.api.po.GoodsDetailPO;
import com.example.api.service.BrowsingHistoryService;
import com.example.api.service.GoodsService;
import com.example.api.vo.*;
import com.example.common.bo.PageParamBO;
import com.example.common.po.GoodsPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

/**
 *
 */
@Service("apiGoodsServiceImpl")
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, GoodsPO>
        implements GoodsService {
    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private BrowsingHistoryService browsingHistoryService;

    /**
     * 分页
     *
     * @param pageParamBO
     * @param goodsPaginateDTO
     * @return
     */
    @Override
    public IPage getPaginateJoinByMap(PageParamBO pageParamBO, GoodsPaginateDTO goodsPaginateDTO) {
        return goodsMapper.getPaginateJoinByMap(new Page<>(pageParamBO.getPageIndex(), pageParamBO.getPageSize()), goodsPaginateDTO).convert(po -> {
            GoodsPaginateVO vo = new GoodsPaginateVO();
            BeanUtils.copyProperties(po, vo);
            return vo;
        });
    }

    /**
     * 详情
     *
     * @param goodsId
     * @return
     */
    @Override
    public GoodsDetailVO getDetail(Integer goodsId) {
        Integer userId = (Integer) httpServletRequest.getSession().getAttribute("id");
        GoodsDetailPO po = goodsMapper.getDetail(userId, goodsId);
        GoodsDetailVO vo = new GoodsDetailVO();
        BeanUtils.copyProperties(po, vo);
        vo.setGoodsGallery(JSON.parseArray(po.getGoodsGallery()));
        // 组装属性vo
        vo.setAttr(po.getAttr().stream().map(p -> {
            GoodsDetailAttrVO v = new GoodsDetailAttrVO();
            BeanUtils.copyProperties(p, v);
            v.setAttrValues(p.getAttrValues().stream().map(pp -> {
                GoodsDetailAttrValueVO vv = new GoodsDetailAttrValueVO();
                BeanUtils.copyProperties(pp, vv);
                return vv;
            }).collect(Collectors.toList()));
            return v;
        }).collect(Collectors.toList()));
        // 组装规格vo
        vo.setSpec(po.getSpec().stream().map(p -> {
            GoodsDetailSpecVO v = new GoodsDetailSpecVO();
            BeanUtils.copyProperties(p, v);
            v.setGallery(JSON.parseArray(p.getGallery()));
            return v;
        }).collect(Collectors.toList()));
        // 插入记录
        BrowsingHistoryBO browsingHistoryBO = new BrowsingHistoryBO();
        BeanUtils.copyProperties(vo, browsingHistoryBO);
        browsingHistoryBO.setUserId(userId);
        browsingHistoryService.add(browsingHistoryBO);
        return vo;
    }
}




