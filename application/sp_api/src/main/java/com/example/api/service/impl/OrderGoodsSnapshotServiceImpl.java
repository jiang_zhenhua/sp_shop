package com.example.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.api.service.OrderGoodsSnapshotService;
import com.example.common.mapper.OrderGoodsSnapshotMapper;
import com.example.common.po.OrderGoodsSnapshotPO;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service("apiOrderGoodsSnapshotServiceImpl")
public class OrderGoodsSnapshotServiceImpl extends ServiceImpl<OrderGoodsSnapshotMapper, OrderGoodsSnapshotPO>
        implements OrderGoodsSnapshotService {

}




