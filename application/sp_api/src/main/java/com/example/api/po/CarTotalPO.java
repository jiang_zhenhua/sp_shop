package com.example.api.po;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarTotalPO {
    @ApiModelProperty(value = "总数量", example = "1")
    private Integer totalQuantity;

    @ApiModelProperty(value = "选中数量", example = "1")
    private Integer selectedQuantity;

    @ApiModelProperty(value = "选中商品合计金额", example = "10.00")
    private BigDecimal totalAmount;
}
