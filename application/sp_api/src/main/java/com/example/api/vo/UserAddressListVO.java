package com.example.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAddressListVO {
    @ApiModelProperty(value = "主键", example = "1")
    private Integer addressId;

    @ApiModelProperty(value = "省id", example = "1")
    private Integer provinceId;

    @ApiModelProperty(value = "省名称", example = "上海市")
    private String provinceName;

    @ApiModelProperty(value = "市id", example = "1")
    private Integer cityId;

    @ApiModelProperty(value = "市名称", example = "上海市")
    private String cityName;

    @ApiModelProperty(value = "区id", example = "1")
    private Integer districtId;

    @ApiModelProperty(value = "区名称", example = "浦东新区")
    private String districtName;

    @ApiModelProperty(value = "收货人名称", example = "半夏")
    private String name;

    @ApiModelProperty(value = "收货人手机号", example = "13800000001")
    private String mobile;

    @ApiModelProperty(value = "收货地址", example = "迪士尼")
    private String fullAddress;

    @ApiModelProperty(value = "是否默认 0非 1真【需要确保只有一个】", example = "true")
    private Boolean isDefault;
}
