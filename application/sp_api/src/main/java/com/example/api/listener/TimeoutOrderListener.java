package com.example.api.listener;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TimeoutOrderListener {
    @Autowired
    private com.example.common.service.OrderService OrderService;
    /**
     * 处理死信订单
     * @param orderId
     */
    @RabbitListener(queues = "q.order.dlx")
    public void onMessage(Integer orderId, Channel channel, Message message) throws IOException {
        OrderService.autoCancel(orderId,channel,message);
    }
}
