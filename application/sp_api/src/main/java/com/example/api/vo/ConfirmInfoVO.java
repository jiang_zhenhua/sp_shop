package com.example.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 获取到的确认信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmInfoVO {
    @ApiModelProperty(value = "购物车选中内容", example = "")
    List<ConfirmInfoGoodsVO> list;

    @ApiModelProperty(value = "收货地址信息", example = "")
    private UserAddressDetailVO userAddress;
}
