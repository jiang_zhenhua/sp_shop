package com.example.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.vo.CarTotalVO;
import com.example.api.dto.CarAddDTO;
import com.example.api.dto.CarEditQuantityDTO;
import com.example.common.bo.PageParamBO;
import com.example.common.po.CarPO;

import java.util.List;

/**
 *
 */
public interface CarService extends IService<CarPO> {
    IPage getJoinPaginateById(PageParamBO pageParamBO);

    CarTotalVO add(CarAddDTO carAddDto);

    CarTotalVO editQuantity(CarEditQuantityDTO carEditQuantityDto);

    CarTotalVO editChecked(Integer carId);

    Integer getCount();

    CarTotalVO getCartTotal();

    CarTotalVO delete(Integer carId);

    CarTotalVO changeAll(Boolean checked);

    CarTotalVO deleteBatch(List carIds);
}
