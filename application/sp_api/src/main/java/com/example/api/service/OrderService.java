package com.example.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.vo.ConfirmInfoVO;
import com.example.api.vo.CreateOrderVO;
import com.example.api.vo.OrderDetailVO;
import com.example.api.dto.CreateOrderDTO;
import com.example.api.dto.GetConfirmInfoDTO;
import com.example.api.dto.OrderPaginateDTO;
import com.example.common.bo.PageParamBO;
import com.example.common.po.OrderPO;

/**
 *
 */
public interface OrderService extends IService<OrderPO> {
    IPage getPaginateJoinByMap(PageParamBO pageParamBO, OrderPaginateDTO orderPaginateDTO);

    OrderDetailVO getDetailJoinById(Integer orderId);

    ConfirmInfoVO getConfirmInfo(GetConfirmInfoDTO getConfirmInfoDTO);

    CreateOrderVO createOrder(CreateOrderDTO createOrderDto);

    void pay(Integer orderId);

    Integer cancel(Integer orderId);
}
