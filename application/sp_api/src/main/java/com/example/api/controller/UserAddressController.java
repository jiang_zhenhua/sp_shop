package com.example.api.controller;

import com.example.api.vo.UserAddressDetailVO;
import com.example.api.vo.UserAddressListVO;
import com.example.api.dto.UserAddressAddDTO;
import com.example.api.dto.UserAddressEditDTO;
import com.example.api.service.UserAddressService;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/userAddress")
@Api(tags = "收货地址")
public class UserAddressController {
    @Autowired
    private UserAddressService userAddressService;

    /**
     * 获取收货地址
     *
     * @return
     */
    @ApiOperation("获取收货地址")
    @GetMapping("/getList")
    private ResultDataVO<List<UserAddressListVO>> getList() {
        return ResultDataVO.success(userAddressService.getList());
    }

    /**
     * 添加
     *
     * @param userAddressAddDTO
     * @return
     */
    @ApiOperation("添加")
    @PostMapping("/add")
    private ResultDataVO add(@Validated @RequestBody UserAddressAddDTO userAddressAddDTO) {
        if (userAddressService.add(userAddressAddDTO) != 0) {
            return ResultDataVO.success(0, "添加成功");
        } else {
            return ResultDataVO.fail(106, "添加失败");
        }
    }

    /**
     * 获取详情
     *
     * @param addressId
     * @return
     */
    @ApiOperation("详情")
    @GetMapping("/detail/{id}")
    private ResultDataVO<UserAddressDetailVO> detail(
            @ApiParam(name = "id", value = "地址id", required = true) @PathVariable("id") Integer addressId
    ) {
        return ResultDataVO.success(userAddressService.detail(addressId));
    }

    /**
     * 编辑
     *
     * @param userAddressEditDTO
     * @return
     */
    @ApiOperation("编辑")
    @PostMapping("/edit")
    private ResultDataVO edit(@Validated @RequestBody UserAddressEditDTO userAddressEditDTO) {
        if (userAddressService.edit(userAddressEditDTO) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 修改默认
     *
     * @param addressId
     * @return
     */
    @ApiOperation("修改默认")
    @GetMapping("/editDefault/{id}")
    private ResultDataVO editDefault(
            @ApiParam(name = "id", value = "地址id", required = true) @PathVariable("id") Integer addressId
    ) {
        if (userAddressService.editDefault(addressId) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 删除
     *
     * @param addressId
     * @return
     */
    @ApiOperation("删除")
    @GetMapping("/delete/{id}")
    private ResultDataVO delete(
            @ApiParam(name = "id", value = "地址id", required = true) @PathVariable("id") Integer addressId) {
        if (userAddressService.delete(addressId) != 0) {
            return ResultDataVO.success(0, "删除成功");
        } else {
            return ResultDataVO.fail(106, "删除失败");
        }
    }
}
