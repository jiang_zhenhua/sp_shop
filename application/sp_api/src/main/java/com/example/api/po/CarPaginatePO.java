package com.example.api.po;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarPaginatePO {
    @ApiModelProperty(value = "购物车ID", example = "1")
    private Integer carId;

    @ApiModelProperty(value = "商品ID", example = "1")
    private Integer goodsId;

    @ApiModelProperty(value = "数量", example = "1")
    private Integer quantity;

    @ApiModelProperty(value = "选中状态", example = "ture")
    private Boolean checked;

    @ApiModelProperty(value = "商品名称", example = "半夏")
    private String goodsName;

    @ApiModelProperty(value = "组合名称", example = "半夏")
    private String keyName;

    @ApiModelProperty(value = "选中商品合计金额", example = "10")
    private BigDecimal price;

    @ApiModelProperty(value = "商品sku", example = "ture")
    private String skuCode;

    @ApiModelProperty(value = "组合封面图", example = "url")
    private String cover;

    @ApiModelProperty(value = "库存数量", example = "1")
    private Integer storeCount;

    @ApiModelProperty(value = "是否上架 0未上架 1已上架", example = "ture")
    private Boolean status;
}
