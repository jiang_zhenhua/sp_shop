package com.example.api.controller;

import com.example.api.dto.GoodsCollectAddDTO;
import com.example.api.dto.GoodsCollectCancelDTO;
import com.example.api.service.GoodsCollectService;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/goodsCollect")
@Api(tags = "商品收藏")
public class GoodsCollectController {

    @Autowired
    private GoodsCollectService goodsCollectService;

    /**
     * 添加收藏
     *
     * @return
     */
    @ApiOperation("添加")
    @PostMapping("/add")
    private ResultDataVO add(@Validated @RequestBody GoodsCollectAddDTO goodsCollectAddDTO) {
        if (goodsCollectService.add(goodsCollectAddDTO) != 0) {
            return ResultDataVO.success(0, "收藏成功");
        } else {
            return ResultDataVO.fail(106, "收藏失败");
        }
    }

    /**
     * 取消收藏
     *
     * @return
     */
    @ApiOperation("取消")
    @PostMapping("/cancel")
    private ResultDataVO cancel(@Validated @RequestBody GoodsCollectCancelDTO goodsCollectCancelDTO) {
        if (goodsCollectService.cancel(goodsCollectCancelDTO) != 0) {
            return ResultDataVO.success(0, "取消成功");
        } else {
            return ResultDataVO.fail(106, "取消失败");
        }
    }
}
