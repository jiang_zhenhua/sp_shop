package com.example.api.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.api.dto.CreateOrderDTO;
import com.example.api.dto.GetConfirmInfoDTO;
import com.example.api.dto.OrderPaginateDTO;
import com.example.api.service.OrderService;
import com.example.api.vo.*;
import com.example.common.bo.PageParamBO;
import com.example.common.vo.PageResultVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController("ApiOrderController")
@RequestMapping("/order")
@Api(tags = "订单")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 分页
     *
     * @param pageIndex
     * @param pageSize
     * @param orderPaginateDTO
     * @return
     */
    @ApiOperation("分页")
    @PostMapping("/paginate")
    private ResultDataVO<List<OrderPaginateVO>> paginate(
            @ApiParam(name = "pageIndex", value = "当前页数", required = true) @RequestParam Integer pageIndex,
            @ApiParam(name = "pageSize", value = "每页显示条目个数", required = true) @RequestParam Integer pageSize,
            @RequestBody OrderPaginateDTO orderPaginateDTO
    ) {
        IPage page = orderService.getPaginateJoinByMap(new PageParamBO(pageIndex, pageSize), orderPaginateDTO);
        //构建响应对象
        return ResultDataVO.success(page.getRecords(), new PageResultVO(page));
    }

    /**
     * 详情
     *
     * @param orderId
     * @return
     */
    @ApiOperation("详情")
    @GetMapping("/detail/{id}")
    private ResultDataVO<OrderDetailVO> detail(
            @ApiParam(name = "id", value = "订单id", required = true) @PathVariable("id") Integer orderId
    ) {
        return ResultDataVO.success(orderService.getDetailJoinById(orderId));
    }

    /**
     * 获取订单确认信息
     *
     * @param getConfirmInfoDTO
     * @return
     */
    @ApiOperation("获取订单确认信息")
    @PostMapping("/getConfirmInfo")
    private ResultDataVO<ConfirmInfoVO> getConfirmInfo(@RequestBody(required = false) GetConfirmInfoDTO getConfirmInfoDTO) {
        return ResultDataVO.success(orderService.getConfirmInfo(getConfirmInfoDTO));
    }

    /**
     * 创建订单
     *
     * @param CreateOrderDto
     * @return
     */
    @ApiOperation("创建订单")
    @PostMapping("/createOrder")
    private ResultDataVO<CreateOrderVO> createOrder(
            @RequestBody(required = false) CreateOrderDTO CreateOrderDto
    ) {
        CreateOrderVO createOrderVO = orderService.createOrder(CreateOrderDto);
        if (!Objects.isNull(createOrderVO.getOrderId())) {
            return ResultDataVO.success(createOrderVO, "创建成功");
        } else {
            return ResultDataVO.fail(106, "创建失败");
        }
    }
    @ApiOperation("支付")
    @GetMapping("/pay/{id}")
    private ResultDataVO pay(
            @ApiParam(name = "id", value = "订单id", required = true) @PathVariable("id") Integer orderId
    ){
        orderService.pay(orderId);
        return null;
    }

    /**
     * 取消订单
     *
     * @param orderId
     * @return
     */
    @ApiOperation("取消订单")
    @GetMapping("/cancel/{id}")
    private ResultDataVO cancel(
            @ApiParam(name = "id", value = "订单id", required = true) @PathVariable("id") Integer orderId
    ) {
        if (orderService.cancel(orderId) != 0) {
            return ResultDataVO.success(0, "取消成功");
        } else {
            return ResultDataVO.fail(106, "取消失败");
        }
    }

}
