package com.example.api.po;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarListPO {
    @ApiModelProperty(value = "购物车ID", example = "1")
    private Integer carId;

    @ApiModelProperty(value = "商品ID", example = "1")
    private Integer goodsId;

    @ApiModelProperty(value = "数量", example = "1")
    private Integer quantity;

    @ApiModelProperty(value = "商品名称", example = "半夏")
    private String goodsName;

    @ApiModelProperty(value = "组合名称", example = "半夏")
    private String keyName;

    @ApiModelProperty(value = "商品轮播图片", example = "url")
    private String goodsGallery;

    @ApiModelProperty(value = "商品轮详细介绍", example = "url")
    private String goodsContent;

    @ApiModelProperty(value = "详细介绍", example = "")
    private String content;

    @ApiModelProperty(value = "折扣价", example = "10")
    private BigDecimal oldPrice;

    @ApiModelProperty(value = "选中商品合计金额", example = "10")
    private BigDecimal price;

    @ApiModelProperty(value = "商品sku", example = "ture")
    private String skuCode;

    @ApiModelProperty(value = "条形码", example = "152ae88d1c3")
    private String barCode;

    @ApiModelProperty(value = "组合封面图", example = "url")
    private String cover;

    @ApiModelProperty(value = "规格轮播", example = "url")
    private String gallery;

    @ApiModelProperty(value = "库存数量", example = "1")
    private Integer storeCount;

}
