package com.example.api.po;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 获取到的确认信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsSpecCreateOrderPO {
    @ApiModelProperty(value = "商品id", example = "1")
    private Integer goodsId;

    @ApiModelProperty(value = "商品名称", example = "半夏")
    private String goodsName;

    @ApiModelProperty(value = "商品相册", example = "")
    private String goodsGallery;

    @ApiModelProperty(value = "商品内容", example = "")
    private String goodContent;

    @ApiModelProperty(value = "规格相册", example = "")
    private String gallery;

    @ApiModelProperty(value = "详情", example = "")
    private String content;

    @ApiModelProperty(value = "折扣前价格", example = "10.00")
    private BigDecimal OldPrice;

    @ApiModelProperty(value = "规格名称", example = "f85698f3-95fa-40c4-8ef3-5773d5b537b4")
    private String keyName;

    @ApiModelProperty(value = "封面", example = "url")
    private String cover;

    @ApiModelProperty(value = "价格", example = "10.00")
    private BigDecimal price;

    @ApiModelProperty(value = "库存数量", example = "10")
    private Integer storeCount;

    @ApiModelProperty(value = "是否上架 0未上架 1已上架", example = "ture")
    private Boolean status;

    @ApiModelProperty(value = "商品码", example = "bc59cc42-2ed2-4d38-80e0-bee156fc880f")
    private String skuCode;

    @ApiModelProperty(value = "条码", example = "bee156fc880f")
    private String barCode;
}
