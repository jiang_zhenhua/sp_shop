package com.example.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.api.po.GoodsSpecConfirmInfoPO;
import com.example.api.po.GoodsSpecCreateOrderPO;
import com.example.common.po.GoodsSpecPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Entity com.example.common.model.GoodsSpec
 */
@Mapper
@Repository("apiGoodsSpecMapper")
public interface GoodsSpecMapper extends BaseMapper<GoodsSpecPO> {
    GoodsSpecConfirmInfoPO getGoodsSpecConfirmInfo(
            @Param("goodsId") Integer goodsId,
            @Param("skuCode") String skuCode
    );

    GoodsSpecCreateOrderPO getGoodsSpecCreateOrder(
            @Param("goodsId") Integer goodsId,
            @Param("skuCode") String skuCode
    );

}




