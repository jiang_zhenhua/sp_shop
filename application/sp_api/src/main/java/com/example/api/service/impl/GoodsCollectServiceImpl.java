package com.example.api.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.api.dto.GoodsCollectAddDTO;
import com.example.api.dto.GoodsCollectCancelDTO;
import com.example.common.mapper.GoodsCollectMapper;
import com.example.api.service.GoodsCollectService;
import com.example.common.exception.ServiceException;
import com.example.common.po.GoodsCollectPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 *
 */
@Service("apiGoodsCollectServiceImpl")
public class GoodsCollectServiceImpl extends ServiceImpl<GoodsCollectMapper, GoodsCollectPO>
        implements GoodsCollectService {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private GoodsCollectMapper goodsCollectMapper;

    /**
     * 添加商品收藏
     *
     * @param goodsCollectAddDTO
     * @return
     */
    @Override
    public Integer add(GoodsCollectAddDTO goodsCollectAddDTO) {
        // 判断是否已经添加
        Integer userId = (Integer) httpServletRequest.getSession().getAttribute("id");
        GoodsCollectPO data = goodsCollectMapper.selectOne(
                new QueryWrapper<GoodsCollectPO>()
                        .lambda()
                        .eq(GoodsCollectPO::getUserId, userId)
                        .eq(GoodsCollectPO::getGoodsId, goodsCollectAddDTO.getGoodsId())
        );
        if (Objects.isNull(data)) {
            GoodsCollectPO po = new GoodsCollectPO();
            BeanUtils.copyProperties(goodsCollectAddDTO, po);
            po.setUserId(userId);
            return goodsCollectMapper.insert(po);
        } else {
            GoodsCollectPO po = new GoodsCollectPO();
            BeanUtils.copyProperties(goodsCollectAddDTO, po);
            po.setUserId(userId);
            po.setDeleteTime(0);
            return goodsCollectMapper.update(
                    po,
                    new UpdateWrapper<GoodsCollectPO>()
                            .lambda()
                            .eq(GoodsCollectPO::getUserId, userId)
            );
        }
    }

    /**
     * 取消商品收藏
     *
     * @param goodsCollectCancelDTO
     * @return
     */
    @Override
    public Integer cancel(GoodsCollectCancelDTO goodsCollectCancelDTO) {
        // 判断是否已经添加
        Integer userId = (Integer) httpServletRequest.getSession().getAttribute("id");
        GoodsCollectPO data = goodsCollectMapper.selectOne(
                new QueryWrapper<GoodsCollectPO>()
                        .lambda()
                        .eq(GoodsCollectPO::getUserId, userId)
                        .eq(GoodsCollectPO::getGoodsId, goodsCollectCancelDTO.getGoodsId())
        );
        if (Objects.isNull(data)) {
            throw new ServiceException("找不到资源", 104);
        }
        GoodsCollectPO po = new GoodsCollectPO();
        BeanUtils.copyProperties(goodsCollectCancelDTO, po);
        po.setDeleteTime((int) (System.currentTimeMillis() / 1000));
        return goodsCollectMapper.update(
                po,
                new UpdateWrapper<GoodsCollectPO>()
                        .lambda()
                        .eq(GoodsCollectPO::getUserId, userId)
        );
    }
}




