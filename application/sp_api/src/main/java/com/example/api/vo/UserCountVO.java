package com.example.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserCountVO {
    @ApiModelProperty(value = "待支付订单数量", example = "1")
    private Integer waitPayCount;

    @ApiModelProperty(value = "统计代发货数量", example = "1")
    private Integer waitSendCount;

    @ApiModelProperty(value = "待收货数量", example = "1")
    private Integer waitTakeCount;

    @ApiModelProperty(value = "退换售后数量", example = "1")
    private Integer afterSales;

    @ApiModelProperty(value = "获取收藏数量", example = "1")
    private Integer collectCount;

    @ApiModelProperty(value = "浏览记录数量", example = "1")
    private Integer browsingHistoryCount;
}
