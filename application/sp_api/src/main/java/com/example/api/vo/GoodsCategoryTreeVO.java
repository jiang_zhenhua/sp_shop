package com.example.api.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsCategoryTreeVO {
    @ApiModelProperty(value = "分类id", example = "1")
    private Integer catId;

    @ApiModelProperty(value = "上级id", example = "0")
    private Integer pid;

    @ApiModelProperty(value = "分类名称", example = "name")
    private String catName;

    @ApiModelProperty(value = "等级", example = "1")
    private Integer catLevel;

    @ApiModelProperty(value = "路径", example = ",1,")
    private String catPath;

    @ApiModelProperty(value = "下级", example = "0")
    private List<GoodsCategoryTreeVO> children;

    /**
     * 转换vo
     * @param trees
     * @return
     */
    public static List<GoodsCategoryTreeVO> convertToVO(List<Tree<String>> trees) {
        return Objects.isNull(trees)?new ArrayList<>():CollUtil.newArrayList(
                trees.stream()
                        .map(tree -> convert(tree))
                        .toArray(GoodsCategoryTreeVO[]::new)
        );
    }

    /**
     * 递归
     * @param node
     * @return
     */
    private static GoodsCategoryTreeVO convert(Tree<String> node) {
        GoodsCategoryTreeVO vo = new GoodsCategoryTreeVO();
        vo.setCatId(Integer.parseInt((String) node.get("catId")));
        vo.setPid(Integer.parseInt((String) node.get("pid")));
        vo.setCatName((String) node.get("catName"));
        vo.setCatPath((String) node.get("catPath"));
        vo.setCatLevel((Integer) node.get("catLevel"));
        vo.setChildren((List<GoodsCategoryTreeVO>) node.get("children"));
        if (!Objects.isNull(node.getChildren()) && node.getChildren().size()>0) {
            vo.setChildren(CollUtil.newArrayList(
                    node.getChildren().stream()
                            .map(child -> convert(child))
                            .toArray(GoodsCategoryTreeVO[]::new)
            ));
        }
        return vo;
    }
}
