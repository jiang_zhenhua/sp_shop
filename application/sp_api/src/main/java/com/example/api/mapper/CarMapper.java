package com.example.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.api.po.CarListPO;
import com.example.api.po.CarPaginatePO;
import com.example.api.po.CarTotalPO;
import com.example.common.po.CarPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Entity com.example.common.model.Car
 */
@Mapper
@Repository("apiCarMapper")
public interface CarMapper extends BaseMapper<CarPO> {
    IPage getJoinPaginateById(IPage page, @Param("userId") Integer userId);

    Integer getCount(@Param("userId") Integer userId);

    CarTotalPO getCartTotal(@Param("userId") Integer userId);

    List<CarPaginatePO> getConfirmInfo(@Param("userId") Integer userId);

    List<CarListPO> selectByMapJoinGoods(@Param("userId") Integer userId);
}




