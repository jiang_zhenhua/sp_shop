package com.example.api.po;

import com.example.common.enums.OrderStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderPaginatePO {
    @ApiModelProperty(value = "订单id", example = "1")
    private Integer orderId;

    @ApiModelProperty(value = "订单号", example = "SN17119511310600395")
    private String orderCode;

    @ApiModelProperty(value = "订单金额", example = "1")
    private BigDecimal orderPrice;

    @ApiModelProperty(value = "支付金额", example = "1")
    private BigDecimal paymentAmount;

    @ApiModelProperty(value = "支付状态", example = "未支付/待发货/待收货/已完成/取消")
    private OrderStatus orderStatus;

    @ApiModelProperty(value = "创建时间", example = "1711954513")
    private Integer createTime;

    @ApiModelProperty(value = "快照", example = "")
    List<OrderPaginateGoodsSnapshotPO> orderGoodsSnapshot;
}
