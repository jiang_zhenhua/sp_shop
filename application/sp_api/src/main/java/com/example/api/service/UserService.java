package com.example.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.vo.UserCountVO;
import com.example.common.po.UserPO;

/**
 *
 */
public interface UserService extends IService<UserPO> {
    UserCountVO getCount();
}
