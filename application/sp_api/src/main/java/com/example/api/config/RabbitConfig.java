package com.example.api.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
@Configuration
public class RabbitConfig {

    @Value("${app.order.holdTime}")
    private Integer holdTime; //保留订单时间

    /**
     * 普通订单队列
     * @return
     */
    @Bean
    public Queue queue() {
        Map<String, Object> props = new HashMap<>();
        // 消息的生存时间,时间到加入死信队列
        props.put("x-message-ttl", holdTime*1000);
        // 设置该队列所关联的死信交换器（当队列消息到期后依然没有消费，则加入死信 队列）
        props.put("x-dead-letter-exchange", "ex.order.dlx");
        // 设置该队列所关联的死信交换器的routingKey，如果没有特殊指定，使用原队列的routingKey
        props.put("x-dead-letter-routing-key", "order.dlx");
        Queue queue = new Queue("q.order", true, false, false, props);
        return queue;
    }

    /**
     * 订单死信队列
     * @return
     */
    @Bean
    public Queue queueDlx() {
        Queue queue = new Queue("q.order.dlx", true, false, false);
        return queue;
    }

    /**
     * 订单队列交换机
     * @return
     */
    @Bean
    public Exchange exchange() {
        DirectExchange exchange = new DirectExchange("ex.order", true,false, null);
        return exchange;
    }
    /**
     * 订单死信交换器
     * @return
     */
    @Bean
    public Exchange exchangeDlx() {
        DirectExchange exchange = new DirectExchange("ex.order.dlx", true,false, null);
        return exchange;
    }

    /**
     * 绑定订单交换机
     * @return
     */
    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(exchange()).with("order").noargs();
    }

    /**
     * 绑定订单死信交换机
     * @return
     */
    @Bean
    public Binding bindingDlx() {
        return BindingBuilder.bind(queueDlx()).to(exchangeDlx()).with("order.dlx").noargs();
    }
}
