package com.example.api.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.api.vo.GoodsDetailVO;
import com.example.api.vo.GoodsPaginateVO;
import com.example.api.dto.GoodsPaginateDTO;
import com.example.api.service.GoodsService;
import com.example.common.bo.PageParamBO;
import com.example.common.vo.PageResultVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("ApiGoodsController")
@RequestMapping("/goods")
@Api(tags = "商品")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    /**
     * 获取分页
     *
     * @return
     */
    @ApiOperation("分页")
    @PostMapping("/paginate")
    private ResultDataVO<List<GoodsPaginateVO>> paginate(
            @ApiParam(name = "pageIndex", value = "当前页数", required = true) @RequestParam Integer pageIndex,
            @ApiParam(name = "pageSize", value = "每页显示条目个数", required = true) @RequestParam Integer pageSize,
            @RequestBody GoodsPaginateDTO goodsPaginateDTO
    ) {
        IPage page = goodsService.getPaginateJoinByMap(new PageParamBO(pageIndex, pageSize), goodsPaginateDTO);
        //构建响应对象
        return ResultDataVO.success(page.getRecords(), new PageResultVO(page));
    }

    /**
     * 获取商品详情
     *
     * @param goodsId
     * @return
     */
    @ApiOperation("详情")
    @GetMapping("/detail/{id}")
    private ResultDataVO<GoodsDetailVO> detail(
            @ApiParam(name = "id", value = "商品id", required = true) @PathVariable("id") Integer goodsId
    ) {
        return ResultDataVO.success(goodsService.getDetail(goodsId));
    }
}
