package com.example.api.po;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsPaginatePO {
    @ApiModelProperty(value = "商品id", example = "1")
    private Integer goodsId;

    @ApiModelProperty(value = "商品名称", example = "半夏")
    private String goodsName;

    @ApiModelProperty(value = "商品封面", example = "url")
    private String goodsCover;

    @ApiModelProperty(value = "是否热门", example = "true")
    private Boolean isHot;

    @ApiModelProperty(value = "是否新品", example = "true")
    private Boolean isNew;

    @ApiModelProperty(value = "商品最低价", example = "10.00")
    private BigDecimal price;
}
