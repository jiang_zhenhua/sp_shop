package com.example.api.controller;

import com.example.api.service.GoodsCategoryService;
import com.example.api.vo.GoodsCategoryTreeVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("ApiGoodsCategoryService")
@RequestMapping("/goodsCategory")
@Api(tags = "商品分类")
public class GoodsCategoryController {
    @Autowired
    public GoodsCategoryService goodsCategoryService;

    /**
     * 树形结构
     *
     * @return
     */
    @ApiOperation("树形列表")
    @GetMapping("/getTree")
    public ResultDataVO<List<GoodsCategoryTreeVO>> getTree() {
        return ResultDataVO.success(goodsCategoryService.getTree());
    }
}
