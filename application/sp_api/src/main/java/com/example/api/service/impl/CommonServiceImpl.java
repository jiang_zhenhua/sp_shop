package com.example.api.service.impl;

import com.example.api.dto.CommonGetMesDTO;
import com.example.api.service.CommonService;
import com.example.common.utils.MobileCaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 登录
 */
@Service("apiCommonServiceImpl")
public class CommonServiceImpl implements CommonService {
    @Autowired
    MobileCaptchaUtil mobileCaptchaUtil;

    @Override
    public void getMes(CommonGetMesDTO commonGetMesDTO) throws Exception {
        mobileCaptchaUtil.getCaptcha(commonGetMesDTO.getMobile());
    }
}
