package com.example.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.api.vo.GoodsDetailVO;
import com.example.api.dto.GoodsPaginateDTO;
import com.example.common.bo.PageParamBO;
import com.example.common.po.GoodsPO;

/**
 *
 */
public interface GoodsService extends IService<GoodsPO> {
    IPage getPaginateJoinByMap(PageParamBO pageParamBO, GoodsPaginateDTO goodsPaginateDTO);

    GoodsDetailVO getDetail(Integer goodsId);
}
