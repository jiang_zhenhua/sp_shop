package com.example.api.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.api.service.BrowsingHistoryService;
import com.example.api.vo.BrowsingHistoryVO;
import com.example.common.bo.PageParamBO;
import com.example.common.vo.PageResultVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/browsingHistory")
@Api(tags = "浏览记录")
public class BrowsingHistoryController {
    @Autowired
    private BrowsingHistoryService browsingHistoryService;

    /**
     * 获取分页
     *
     * @return
     */
    @ApiOperation("分页")
    @PostMapping("/paginate")
    private ResultDataVO<List<BrowsingHistoryVO>> paginate(
            @ApiParam(name = "pageIndex", value = "当前页数", required = true) @RequestParam Integer pageIndex,
            @ApiParam(name = "pageSize", value = "每页显示条目个数", required = true) @RequestParam Integer pageSize
    ) {
        IPage page = browsingHistoryService.getPaginate(new PageParamBO(pageIndex, pageSize));
        //构建响应对象
        return ResultDataVO.success(page.getRecords(), new PageResultVO(page));
    }
}
