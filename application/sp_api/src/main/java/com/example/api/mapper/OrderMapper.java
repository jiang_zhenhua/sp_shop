package com.example.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.api.dto.OrderPaginateDTO;
import com.example.api.po.OrderDetailPO;
import com.example.api.po.OrderPaginatePO;
import com.example.common.po.OrderPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Entity com.example.common.model.Order
 */
@Mapper
@Repository("apiOrderMapper")
public interface OrderMapper extends BaseMapper<OrderPO> {
    IPage<OrderPaginatePO> getPaginateJoinByMap(
            IPage<OrderPaginatePO> page,
            @Param("userId") Integer userId,
            @Param("request") OrderPaginateDTO orderPaginateDTO
    );

    OrderDetailPO getDetailJoinById(
            @Param("orderId") Integer orderId,
            @Param("userId") Integer userId
    );
}




