package com.example.api.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

/**
 * 浏览记录
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("browsing_history")
public class BrowsingHistoryVO {
    @ApiModelProperty(value = "记录id", example = "1")
    @Id
    private String id;

    @ApiModelProperty(value = "用户id", example = "1")
    private Integer userId;

    @ApiModelProperty(value = "商品id", example = "1")
    private Integer goodsId;

    @ApiModelProperty(value = "商品名称", example = "半夏")
    private String goodsName;

    @ApiModelProperty(value = "商品封面", example = "url")
    private String goodsCover;

    @ApiModelProperty(value = "商品价格", example = "10.00")
    private BigDecimal price;

    @ApiModelProperty(value = "更新时间", example = "1715307549")
    private Integer updateTime;
}
