package com.example.api.controller;

import com.example.api.dto.CommonGetMesDTO;
import com.example.api.service.CommonService;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController("ApiCommonController")
@RequestMapping("/common")
@Api(tags = "通用")
public class CommonController {
    @Autowired
    private CommonService commonService;

    /**
     * 获取手机验证码
     *
     * @param commonGetMesDTO
     * @return
     * @throws Exception
     */
    @ApiOperation("获取手机验证码")
    @PostMapping("/getMes")
    public ResultDataVO getMes(@Validated @RequestBody CommonGetMesDTO commonGetMesDTO) throws Exception {
        commonService.getMes(commonGetMesDTO);
        return ResultDataVO.success(null, "获取成功");
    }

}
