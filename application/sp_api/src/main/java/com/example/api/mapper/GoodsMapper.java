package com.example.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.api.dto.GoodsPaginateDTO;
import com.example.api.po.GoodsDetailPO;
import com.example.api.po.GoodsPaginatePO;
import com.example.common.po.GoodsPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


/**
 * @Entity com.example.common.model.Goods
 */
@Mapper
@Repository("apiGoodsMapper")
public interface GoodsMapper extends BaseMapper<GoodsPO> {
    IPage<GoodsPaginatePO> getPaginateJoinByMap(Page<GoodsPaginatePO> page, @Param("request") GoodsPaginateDTO request);

    GoodsDetailPO getDetail(@Param("userId") Integer userId, @Param("goodsId") Integer goodsId);
}




