package com.example.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.admin.dto.OrderPaginateDTO;
import com.example.admin.vo.OrderDetailVO;
import com.example.common.bo.PageParamBO;
import com.example.common.po.OrderPO;

public interface OrderService extends IService<OrderPO> {
    IPage getPaginate(PageParamBO pageParamBO, OrderPaginateDTO orderPaginateDTO);
    OrderDetailVO detailJoinById(Integer orderId);
}
