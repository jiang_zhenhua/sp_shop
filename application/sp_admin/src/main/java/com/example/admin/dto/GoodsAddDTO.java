package com.example.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品添加
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsAddDTO {
    @ApiModelProperty(value = "商品分类", required = true, example = "1")
    @NotNull(message = "请选择商品分类")
    private Integer catId;

    @ApiModelProperty(value = "商品名称", required = true, example = "手机")
    @NotNull(message = "商品名称不能为空")
    private String goodsName;

    @ApiModelProperty(value = "关键词", example = "iphone")
    private String goodsKeyword;

    @ApiModelProperty(value = "商品单位", required = true, example = "台")
    @NotNull(message = "商品单位不能为空")
    private String unitName;

    @ApiModelProperty(value = "商品封面", required = true, example = "url")
    @NotNull(message = "需要上传商品封面")
    private String goodsCover;

    @ApiModelProperty(value = "画册", example = "上传的图片数组")
    private List goodsGallery;

    @ApiModelProperty(value = "商品简介", example = "好使：)")
    private String goodsDesc;

    @ApiModelProperty(value = "商品详情", example = "好使：)")
    private String goodsContent;

    @ApiModelProperty(value = "是否多规格 0单规格 1多规格", required = true, example = "1")
    private Boolean specType;

    @ApiModelProperty(value = "是否展示", required = true, example = "ture")
    @NotNull(message = "是否展示")
    private Boolean isShow;

    @ApiModelProperty(value = "属性数组", example = "ture")
    private List<GoodsAddAttrDTO> attr;

    @ApiModelProperty(value = "组合规格", required = true, example = "规则数组")
    @NotEmpty(message = "请填写规格信息")
    private List<GoodsAddSpecDTO> spec;
}
