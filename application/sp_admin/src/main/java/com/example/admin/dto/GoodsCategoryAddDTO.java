package com.example.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsCategoryAddDTO {
    @ApiModelProperty(value = "分类名称", required = true, example = "name")
    @NotNull(message = "请填写分类名称")
    private String catName;

    @ApiModelProperty(value = "上级id", required = true, example = "0")
    @NotNull(message = "缺少上级id")
    private Integer pid;

    @ApiModelProperty(value = "排序", required = true, example = "1")
    @NotNull(message = "排序不能为空")
    @Range(min = 0, max = 999, message = "排序只能填写0-999")
    private Integer sort;
}
