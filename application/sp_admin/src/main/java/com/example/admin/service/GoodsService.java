package com.example.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.admin.dto.GoodsAddDTO;
import com.example.admin.dto.GoodsEditDTO;
import com.example.admin.dto.GoodsEditSortDTO;
import com.example.admin.dto.GoodsPaginateDTO;
import com.example.admin.vo.GoodsDetailVO;
import com.example.common.bo.PageParamBO;
import com.example.common.po.GoodsPO;

/**
 *
 */
public interface GoodsService extends IService<GoodsPO> {
    IPage getPaginate(PageParamBO pageParamBO, GoodsPaginateDTO goodsPaginateDTO);
    Boolean add(GoodsAddDTO goodsAddDto);
    GoodsDetailVO detail(Integer goodsId);
    Boolean edit(GoodsEditDTO goodsEditDto);
    Integer editSort(GoodsEditSortDTO goodsEditSortDTO);
    Integer editHot(Integer goodsId);
    Integer editNew(Integer goodsId);
    Integer editShow(Integer goodsId);
    Boolean delete(Integer goodsId);
}
