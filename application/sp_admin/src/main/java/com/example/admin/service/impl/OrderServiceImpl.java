package com.example.admin.service.impl;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.admin.dto.OrderPaginateDTO;
import com.example.admin.mapper.OrderMapper;
import com.example.admin.po.OrderDetailPO;
import com.example.admin.service.OrderService;
import com.example.admin.vo.OrderDetailVO;
import com.example.admin.vo.OrderGoodsSnapshotVO;
import com.example.admin.vo.OrderPaginateVO;
import com.example.common.bo.PageParamBO;
import com.example.common.exception.ServiceException;
import com.example.common.po.OrderPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 商品
 */
@Service("adminOrderServiceImpl")
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderPO>
        implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 分页
     *
     * @param pageParamBO
     * @param orderPaginateDTO
     * @return
     */
    @Override
    public IPage getPaginate(PageParamBO pageParamBO, OrderPaginateDTO orderPaginateDTO) {
        QueryWrapper queryWrapper = new QueryWrapper<OrderPO>()
                .select("order_id", "order_code", "order_price", "payment_amount", "order_status", "create_time")
                .orderByDesc("order_id");
        if (!Objects.isNull(orderPaginateDTO.getOrderCode())) {
            queryWrapper.like("order_code", orderPaginateDTO.getOrderCode());
        }
        if (!Objects.isNull(orderPaginateDTO.getOrderStatus())) {
            queryWrapper.eq("order_status", orderPaginateDTO.getOrderStatus());
        }
        // 查询数据
        return orderMapper.selectPage(
                new Page<>(pageParamBO.getPageIndex(), pageParamBO.getPageSize()),
                queryWrapper
        ).convert(po -> {
            OrderPaginateVO vo = new OrderPaginateVO();
            BeanUtils.copyProperties(po, vo);
            return vo;
        });
    }

    /**
     * 获取详情
     *
     * @param orderId
     * @return
     */
    @Override
    public OrderDetailVO detailJoinById(Integer orderId) {
        OrderDetailPO po = orderMapper.getDetailJoinById(orderId);
        if (Objects.isNull(po)) {
            throw new ServiceException("找不到资源", 104);
        }
        OrderDetailVO vo = new OrderDetailVO();
        BeanUtils.copyProperties(po, vo);
        vo.setOrderGoodsSnapshot(po.getOrderGoodsSnapshot().stream().map(p -> {
            OrderGoodsSnapshotVO v = new OrderGoodsSnapshotVO();
            BeanUtils.copyProperties(p, v);
            v.setGoodsGallery(JSON.parseArray(p.getGoodsGallery()));
            v.setGallery(JSON.parseArray(p.getGallery()));
            return v;
        }).collect(Collectors.toList()));
        return vo;
    }
}




