package com.example.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.admin.dto.OrderPaginateDTO;
import com.example.admin.service.OrderService;
import com.example.admin.vo.OrderDetailVO;
import com.example.common.bo.PageParamBO;
import com.example.common.vo.PageResultVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
@Api(tags = "订单管理")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 分页
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @ApiOperation("分页")
    @PostMapping("/paginate")
    public ResultDataVO paginate(
            @ApiParam(name = "pageIndex", value = "当前页数", required = true) @RequestParam Integer pageIndex,
            @ApiParam(name = "pageSize", value = "每页显示条目个数", required = true) @RequestParam Integer pageSize,
            @RequestBody OrderPaginateDTO orderPaginateDTO
    ) {
        IPage page = orderService.getPaginate(new PageParamBO(pageIndex, pageSize), orderPaginateDTO);
        //构建响应对象
        return ResultDataVO.success(page.getRecords(), new PageResultVO(page));
    }

    /**
     * 获取订单详情
     *
     * @param orderId
     * @return
     */
    @ApiOperation("详情")
    @GetMapping("/detail/{id}")
    public ResultDataVO<OrderDetailVO> paginate(
            @ApiParam(name = "id", value = "订单id", required = true) @PathVariable("id") Integer orderId
    ) {
        return ResultDataVO.success(orderService.detailJoinById(orderId));
    }
}
