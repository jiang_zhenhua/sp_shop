package com.example.admin.dto;

import com.example.common.enums.OrderStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 轮播图分页DTO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderPaginateDTO {
    @ApiModelProperty(value = "订单号", example = "SN17119511165850105")
    private String orderCode;

    @ApiModelProperty(value = "订单状态", example = "WAIT_PAY")
    private OrderStatus orderStatus;
}
