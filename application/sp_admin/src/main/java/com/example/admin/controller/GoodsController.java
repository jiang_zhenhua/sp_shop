package com.example.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.admin.dto.GoodsAddDTO;
import com.example.admin.dto.GoodsEditDTO;
import com.example.admin.dto.GoodsEditSortDTO;
import com.example.admin.dto.GoodsPaginateDTO;
import com.example.admin.service.GoodsService;
import com.example.common.bo.PageParamBO;
import com.example.common.vo.PageResultVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/goods")
@Api(tags = "商品管理")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 商品分页
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @ApiOperation("分页")
    @PostMapping("/paginate")
    public ResultDataVO paginate(
            @ApiParam(name = "pageIndex", value = "当前页数", required = true) @RequestParam Integer pageIndex,
            @ApiParam(name = "pageSize", value = "每页显示条目个数", required = true) @RequestParam Integer pageSize,
            @RequestBody GoodsPaginateDTO goodsPaginateDTO
    ) {
        IPage page = goodsService.getPaginate(new PageParamBO(pageIndex, pageSize), goodsPaginateDTO);
        //构建响应对象
        return ResultDataVO.success(page.getRecords(), new PageResultVO(page));
    }

    /**
     * 添加
     *
     * @param goodsAddDTO
     * @return
     */
    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultDataVO add(@Validated @RequestBody GoodsAddDTO goodsAddDTO) {
        if (goodsService.add(goodsAddDTO)) {
            return ResultDataVO.success(0, "添加成功");
        } else {
            return ResultDataVO.fail(106, "添加失败");
        }
    }

    /**
     * 商品详情
     *
     * @param goodsId
     * @return
     */
    @ApiOperation("商品详情")
    @GetMapping("/detail/{id}")
    public ResultDataVO detail(
            @ApiParam(name = "id", value = "商品id", required = true) @PathVariable("id") Integer goodsId
    ) {
        return ResultDataVO.success(goodsService.detail(goodsId));
    }

    /**
     * 修改商品
     *
     * @param goodsEditDto
     * @return
     */
    @ApiOperation("编辑")
    @PostMapping("/edit")
    public ResultDataVO edit(@Validated @RequestBody GoodsEditDTO goodsEditDto) {
        if (goodsService.edit(goodsEditDto)) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 修改排序
     *
     * @param goodsEditSortDTO
     * @return
     */
    @ApiOperation("修改排序")
    @PostMapping("/editSort")
    public ResultDataVO editSort(@Validated @RequestBody GoodsEditSortDTO goodsEditSortDTO) {
        if (goodsService.editSort(goodsEditSortDTO) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 修改热门状态
     *
     * @param goodsId
     * @return
     */
    @ApiOperation("修改热门状态")
    @GetMapping("/editHot/{id}")
    public ResultDataVO editHot(
            @ApiParam(name = "id", value = "商品id", required = true) @PathVariable("id") Integer goodsId
    ) {
        if (goodsService.editHot(goodsId) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 修改新品状态
     *
     * @param goodsId
     * @return
     */
    @ApiOperation("修改新品状态")
    @GetMapping("/editNew/{id}")
    public ResultDataVO editNew(
            @ApiParam(name = "id", value = "商品id", required = true) @PathVariable("id") Integer goodsId
    ) {
        if (goodsService.editNew(goodsId) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 修改显示状态
     *
     * @param goodsId
     * @return
     */
    @ApiOperation("修改显示状态")
    @GetMapping("/editShow/{id}")
    public ResultDataVO editShow(
            @ApiParam(name = "id", value = "商品id", required = true) @PathVariable("id") Integer goodsId
    ) {
        if (goodsService.editShow(goodsId) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 删除商品
     *
     * @param goodsId
     * @return
     */
    @ApiOperation("删除")
    @GetMapping("/delete/{id}")
    public ResultDataVO delete(
            @ApiParam(name = "id", value = "商品id", required = true) @PathVariable("id") Integer goodsId
    ) {
        if (goodsService.delete(goodsId)) {
            return ResultDataVO.success(0, "删除成功");
        } else {
            return ResultDataVO.fail(106, "删除失败");
        }
    }
}
