package com.example.admin.vo;

import com.example.common.enums.OrderStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderPaginateVO {
    @ApiModelProperty(value = "订单id", example = "1")
    private Integer orderId;

    @ApiModelProperty(value = "订单号", example = "admin")
    private String orderCode;

    @ApiModelProperty(value = "订单金额", example = "10")
    private BigDecimal orderPrice;

    @ApiModelProperty(value = "支付金额", example = "10")
    private BigDecimal paymentAmount;

    @ApiModelProperty(value = "订单状态", example = "WAITPAY")
    private OrderStatus orderStatus;

    @ApiModelProperty(value = "创建时间", example = "1712053702")
    private Integer createTime;
}
