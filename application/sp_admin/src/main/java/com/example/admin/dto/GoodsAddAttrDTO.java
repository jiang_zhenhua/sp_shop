package com.example.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 添加属性
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsAddAttrDTO {
    @ApiModelProperty(value = "属性名称", required = true, example = "颜色")
    @NotNull(message = "请填写属性名称")
    private String attrName;

    @ApiModelProperty(value = "是否展示封面", required = true, example = "true")
    @NotNull(message = "是否展示封面")
    private Boolean setImage;

    @ApiModelProperty(value = "属性值", required = true, example = "属性值数组")
    @NotNull(message = "属性值")
    private List<GoodsAddAttrValueDTO> attrValues;
}
