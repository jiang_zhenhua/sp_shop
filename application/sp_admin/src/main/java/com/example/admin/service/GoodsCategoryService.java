package com.example.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.admin.dto.GoodsCategoryAddDTO;
import com.example.admin.dto.GoodsCategoryEditDTO;
import com.example.admin.dto.GoodsCategoryEditSortDTO;
import com.example.admin.vo.GoodsCategoryDetailVO;
import com.example.admin.vo.GoodsCategoryTreeVO;
import com.example.common.po.GoodsCategoryPO;

import java.util.List;

/**
 *
 */
public interface GoodsCategoryService extends IService<GoodsCategoryPO> {
    List<GoodsCategoryTreeVO> getTree();
    Integer add(GoodsCategoryAddDTO goodsCategoryAddDTO);
    GoodsCategoryDetailVO detail(Integer catId);
    Integer edit(GoodsCategoryEditDTO goodsCategoryEditDTO);
    Integer editSort(GoodsCategoryEditSortDTO goodsCategoryEditSortDTO);
    Integer editShow(Integer catId);
    Integer delete(Integer catId);
}
