package com.example.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsCategoryEditDTO {
    @ApiModelProperty(value = "上级id", required = true, example = "0")
    @NotNull(message = "缺少主键")
    private Integer catId;

    @ApiModelProperty(value = "分类名称", required = true, example = "name")
    @NotNull(message = "请填写分类名称")
    private String catName;
}
