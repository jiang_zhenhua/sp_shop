package com.example.admin.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsPaginateVO {
    @ApiModelProperty(value = "商品id", example = "1")
    private Integer goodsId;

    @ApiModelProperty(value = "商品名称", example = "1")
    private String goodsName;

    @ApiModelProperty(value = "封面图", example = "url")
    private String goodsCover;

    @ApiModelProperty(value = "是否热门", example = "ture")
    private Boolean isHot;

    @ApiModelProperty(value = "是否新品", example = "ture")
    private Boolean isNew;

    @ApiModelProperty(value = "是否显示", example = "ture")
    private Boolean isShow;

    @ApiModelProperty(value = "排序", example = "0")
    private Integer sort;
}
