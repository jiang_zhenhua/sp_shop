package com.example.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsAddAttrValueDTO {
    @ApiModelProperty(value = "商品id", required = true, example = "1")
    @NotNull(message = "缺少商品id")
    private Integer goodsId;

    @ApiModelProperty(value = "属性id", required = true, example = "1")
    @NotNull(message = "缺少属性id")
    private Integer attrId;

    @ApiModelProperty(value = "属性值名称", required = true, example = "红色")
    @NotNull(message = "请填写属性值名称")
    private String attrValueName;

    @ApiModelProperty(value = "属性图片", required = true, example = "url")
    private String attrValueImage;
}