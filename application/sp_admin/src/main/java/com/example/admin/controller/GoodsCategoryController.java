package com.example.admin.controller;

import com.example.admin.dto.GoodsCategoryAddDTO;
import com.example.admin.dto.GoodsCategoryEditDTO;
import com.example.admin.dto.GoodsCategoryEditSortDTO;
import com.example.admin.service.GoodsCategoryService;
import com.example.admin.vo.GoodsCategoryDetailVO;
import com.example.admin.vo.GoodsCategoryTreeVO;
import com.example.common.vo.ResultDataVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/goodsCategory")
@Api(tags = "商品分类")
public class GoodsCategoryController {
    @Autowired
    public GoodsCategoryService goodsCategoryService;

    /**
     * 树形结构
     *
     * @return
     */
    @ApiOperation("树形列表")
    @GetMapping("/getTree")
    public ResultDataVO<List<GoodsCategoryTreeVO>> getTree() {
        return ResultDataVO.success(goodsCategoryService.getTree());
    }

    /**
     * 添加
     *
     * @param goodsCategoryAddDTO
     * @return
     */
    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultDataVO add(@Validated @RequestBody GoodsCategoryAddDTO goodsCategoryAddDTO) {
        if (goodsCategoryService.add(goodsCategoryAddDTO) != 0) {
            return ResultDataVO.success(0, "添加成功");
        } else {
            return ResultDataVO.fail(106, "添加失败");
        }
    }

    /**
     * 详情
     *
     * @param catId
     * @return
     */
    @ApiOperation("详情")
    @GetMapping("/detail/{catId}")
    public ResultDataVO<GoodsCategoryDetailVO> detail(
            @ApiParam(name = "catId", value = "分类id", required = true) @PathVariable("catId") Integer catId
    ) {
        return ResultDataVO.success(goodsCategoryService.detail(catId));
    }

    /**
     * 更新
     *
     * @param goodsCategoryEditDTO
     * @return
     */
    @ApiOperation("更新")
    @PostMapping("/edit")
    public ResultDataVO edit(@Validated @RequestBody GoodsCategoryEditDTO goodsCategoryEditDTO) {
        if (goodsCategoryService.edit(goodsCategoryEditDTO) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 修改排序
     *
     * @param goodsCategoryEditSortDTO
     * @return
     */
    @ApiOperation("修改排序")
    @PostMapping("/editSort")
    public ResultDataVO editSort(@Validated @RequestBody GoodsCategoryEditSortDTO goodsCategoryEditSortDTO) {
        if (goodsCategoryService.editSort(goodsCategoryEditSortDTO) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 修改显示状态
     *
     * @param catId
     * @return
     */
    @ApiOperation("修改显示状态")
    @GetMapping("/editShow/{id}")
    public ResultDataVO editIsShow(
            @ApiParam(name = "id", value = "分类id", required = true) @PathVariable("id") Integer catId
    ) {
        if (goodsCategoryService.editShow(catId) != 0) {
            return ResultDataVO.success(0, "更新成功");
        } else {
            return ResultDataVO.fail(106, "更新失败");
        }
    }

    /**
     * 删除
     *
     * @param catId
     * @return
     */
    @ApiOperation("删除")
    @GetMapping("/delete/{catId}")
    public ResultDataVO delete(
            @ApiParam(name = "id", value = "分类id", required = true) @PathVariable("catId") Integer catId
    ) {
        if (goodsCategoryService.delete(catId) != 0) {
            return ResultDataVO.success(0, "删除成功");
        } else {
            return ResultDataVO.fail(106, "删除失败");
        }
    }
}
