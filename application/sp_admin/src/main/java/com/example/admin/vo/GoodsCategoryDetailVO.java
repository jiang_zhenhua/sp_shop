package com.example.admin.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsCategoryDetailVO {
    @ApiModelProperty(value = "分类id", example = "1")
    private Integer catId;

    @ApiModelProperty(value = "上级id", example = "0")
    private Integer pid;

    @ApiModelProperty(value = "分类名称", example = "name")
    private String catName;

    @ApiModelProperty(value = "等级", example = "1")
    private Integer catLevel;

    @ApiModelProperty(value = "路径", example = ",1,")
    private String catPath;

    @ApiModelProperty(value = "显示状态", example = "true")
    private Boolean isShow;

    @ApiModelProperty(value = "排序", example = "0")
    private Integer sort;
}
