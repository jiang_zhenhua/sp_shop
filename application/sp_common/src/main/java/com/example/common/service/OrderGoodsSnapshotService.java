package com.example.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.po.OrderGoodsSnapshotPO;

/**
 *
 */
public interface OrderGoodsSnapshotService extends IService<OrderGoodsSnapshotPO> {

}
