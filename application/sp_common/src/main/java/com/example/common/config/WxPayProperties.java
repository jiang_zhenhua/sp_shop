package com.example.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "wx.pay.config")
@Data
public class WxPayProperties {

    /**
     * 商户号 mchId;
     */
    private String mchId;

    /**
     * 密钥
     */
    private String mchKey;

    /**
     * appId
     */
    private String appId;
}
