package com.example.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.po.GoodsCategoryPO;

/**
 *
 */
public interface GoodsCategoryService extends IService<GoodsCategoryPO> {

}
