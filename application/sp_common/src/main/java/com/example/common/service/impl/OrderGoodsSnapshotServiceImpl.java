package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.OrderGoodsSnapshotMapper;
import com.example.common.po.OrderGoodsSnapshotPO;
import com.example.common.service.OrderGoodsSnapshotService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class OrderGoodsSnapshotServiceImpl extends ServiceImpl<OrderGoodsSnapshotMapper, OrderGoodsSnapshotPO>
        implements OrderGoodsSnapshotService {

}




