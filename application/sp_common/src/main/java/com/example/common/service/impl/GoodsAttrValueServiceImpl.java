package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.GoodsAttrValueMapper;
import com.example.common.po.GoodsAttrValuePO;
import com.example.common.service.GoodsAttrValueService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class GoodsAttrValueServiceImpl extends ServiceImpl<GoodsAttrValueMapper, GoodsAttrValuePO>
        implements GoodsAttrValueService {

}




