package com.example.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.po.CarPO;

/**
 *
 */
public interface CarService extends IService<CarPO> {

}
