package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.GoodsSpecMapper;
import com.example.common.po.GoodsSpecPO;
import com.example.common.service.GoodsSpecService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class GoodsSpecServiceImpl extends ServiceImpl<GoodsSpecMapper, GoodsSpecPO>
        implements GoodsSpecService {

}




