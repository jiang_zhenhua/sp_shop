package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.GoodsCollectMapper;
import com.example.common.po.GoodsCollectPO;
import com.example.common.service.GoodsCollectService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class GoodsCollectServiceImpl extends ServiceImpl<GoodsCollectMapper, GoodsCollectPO>
        implements GoodsCollectService {

}




