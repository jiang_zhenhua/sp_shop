package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.GoodsMapper;
import com.example.common.po.GoodsPO;
import com.example.common.service.GoodsService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, GoodsPO>
        implements GoodsService {

}




