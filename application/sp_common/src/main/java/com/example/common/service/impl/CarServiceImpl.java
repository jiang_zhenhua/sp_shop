package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.CarMapper;
import com.example.common.po.CarPO;
import com.example.common.service.CarService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class CarServiceImpl extends ServiceImpl<CarMapper, CarPO>
        implements CarService {

}




