package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.UserAddressMapper;
import com.example.common.po.UserAddressPO;
import com.example.common.service.UserAddressService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddressPO>
        implements UserAddressService {

}




