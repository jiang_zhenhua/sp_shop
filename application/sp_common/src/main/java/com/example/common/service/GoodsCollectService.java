package com.example.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.po.GoodsCollectPO;

/**
 *
 */
public interface GoodsCollectService extends IService<GoodsCollectPO> {

}
