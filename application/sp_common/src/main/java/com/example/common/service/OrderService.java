package com.example.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.po.OrderPO;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

import java.io.IOException;

/**
 *
 */
public interface OrderService extends IService<OrderPO> {
    Integer autoCancel(Integer orderId, Channel channel, Message message) throws IOException;
}
