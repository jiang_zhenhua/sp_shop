package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.GoodsCategoryMapper;
import com.example.common.po.GoodsCategoryPO;
import com.example.common.service.GoodsCategoryService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class GoodsCategoryServiceImpl extends ServiceImpl<GoodsCategoryMapper, GoodsCategoryPO>
        implements GoodsCategoryService {

}




