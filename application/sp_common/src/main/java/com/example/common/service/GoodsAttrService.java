package com.example.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.po.GoodsAttrPO;

/**
 *
 */
public interface GoodsAttrService extends IService<GoodsAttrPO> {

}
