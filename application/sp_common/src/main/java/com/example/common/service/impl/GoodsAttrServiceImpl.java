package com.example.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.mapper.GoodsAttrMapper;
import com.example.common.po.GoodsAttrPO;
import com.example.common.service.GoodsAttrService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class GoodsAttrServiceImpl extends ServiceImpl<GoodsAttrMapper, GoodsAttrPO>
        implements GoodsAttrService {

}




